package configwsserver

import (
	"log"
	"math/rand"

	"github.com/spf13/viper"
	"gitlab.com/remotejob/gochatvuewsv3/internal/domains"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/images"
)

type Constants struct {
	PORT         string
	COUNTRY      string
	TARGET       string
	USERAGENT    string
	Imageservice struct {
		Url string
	}

	Wsserver struct {
		Url string
	}
	Staticfiles struct {
		Url string
	}
}

type Config struct {
	Constants
	Wsclients  map[string]domains.Wsclient
	Broadcast  chan domains.Broadcastmsg
	Imgs       []domains.Imgobj
	Defaultimg domains.Imgobj
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	if err != nil {
		return &config, err
	}
	config.Constants = constants

	wsclients := make(map[string]domains.Wsclient)
	config.Wsclients = wsclients
	broadcast := make(chan domains.Broadcastmsg) // broadcast channel
	config.Broadcast = broadcast

	imgs, err := images.Get(config.Imageservice.Url, 9, true)
	if err != nil {
		return &config, err
	}
	config.Imgs = imgs

	config.Defaultimg = imgs[rand.Intn(len(imgs))]

	return &config, err

}

func initViper() (Constants, error) {
	viper.SetConfigName("wsserver.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")               // Search the root directory for the configuration file
	err := viper.ReadInConfig()            // Find and read the config file
	if err != nil {                        // Handle errors reading the config file
		return Constants{}, err
	}

	viper.SetDefault("PORT", "8000")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
