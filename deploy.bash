#scp gochatvuewsv3.config.toml.prod root@142.93.177.207:gochatvuewsv3/gochatvuewsv3.config.toml
scp wsserver.config.toml.prod root@142.93.177.207:gochatvuewsv3/wsserver.config.toml
scp broadcast.config.toml.prod root@142.93.177.207:gochatvuewsv3/broadcast.config.toml

#scp db/datingv3.db root@142.93.177.207:gochatvuewsv3/db/
scp data/hellophrase.csv root@142.93.177.207:gochatvuewsv3/data
scp data/helloagain.csv root@142.93.177.207:gochatvuewsv3/data

go build -o bin/wsserver gitlab.com/remotejob/gochatvuewsv3/cmd/wsserver && \
scp bin/wsserver root@142.93.177.207:/tmp && \
ssh root@142.93.177.207 systemctl stop wsserver.service && \
ssh root@142.93.177.207 mv /tmp/wsserver gochatvuewsv3/ && \
ssh root@142.93.177.207 systemctl start wsserver.service
