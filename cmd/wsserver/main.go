package main

import (
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/rs/cors"
	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/handler"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/handlerMsg"
)

var (
	conf *configwsserver.Config
	err  error
)

func init() {

	conf, err = configwsserver.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}

}

func Routes(configuration *configwsserver.Config) *chi.Mux {
	router := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
	})

	router.Use(
		// render.SetContentType(render.ContentTypeJSON), // Set content-Type headers as application/json
		cors.Handler,
		middleware.Logger, // Log API request calls
		middleware.RedirectSlashes, // Redirect slashes to no slash URL versions
		middleware.Recoverer,       // Recover from panics without crashing server
	)

	router.Route("/v0", func(r chi.Router) {
		r.Mount("/", handler.New(configuration).Routes())
	})

	FileServer(router, "/", conf.Staticfiles.Url)

	return router
}

func main() {

	router := Routes(conf)
	go handlerMsg.Elab(conf)

	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("%s %s\n", method, route) // Walk and print out all routes
		return nil
	}
	if err := chi.Walk(router, walkFunc); err != nil {
		log.Panicf("Logging err: %s\n", err.Error()) // panic if there is an error
	}

	log.Println("Serving application at PORT :" + conf.Constants.PORT)

	s := &http.Server{
		Addr:         conf.Constants.PORT,
		Handler:      router,
		ReadTimeout:  36000 * time.Second,
		WriteTimeout: 36000 * time.Second,
		IdleTimeout:  36000 * time.Second,
	}
	if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatal("Server startup failed", err)
	}

}
func FileServer(r chi.Router, public string, static string) {

	if strings.ContainsAny(public, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	root, _ := filepath.Abs(static)
	if _, err := os.Stat(root); os.IsNotExist(err) {
		panic("Static Documents Directory Not Found")
	}

	fs := http.StripPrefix(public, http.FileServer(http.Dir(root)))

	if public != "/" && public[len(public)-1] != '/' {
		r.Get(public, http.RedirectHandler(public+"/", 301).ServeHTTP)
		public += "/"
	}

	r.Get(public+"*", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		country := r.Header.Get("X-Country")

		log.Println(country)

		if country != conf.COUNTRY {

			redirect(w, r)

		} else {

			file := strings.Replace(r.RequestURI, public, "/", 1)
			if _, err := os.Stat(root + file); os.IsNotExist(err) {
				http.ServeFile(w, r, path.Join(root, "index.html"))
				return
			}
			fs.ServeHTTP(w, r)

		}
	}))
}

func redirect(w http.ResponseWriter, req *http.Request) {

	log.Println("seoproxy", req.RemoteAddr)
	// remove/add not default ports from req.Host
	req.Header.Set("User-Agent", conf.USERAGENT)
	target := conf.TARGET + req.URL.Path
	if len(req.URL.RawQuery) > 0 {
		target += "?" + req.URL.RawQuery
	}
	log.Printf("redirect to: %s", target)
	http.Redirect(w, req, target,
		http.StatusMovedPermanently)

}
