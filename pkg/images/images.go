package images

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/remotejob/gochatvuewsv3/internal/domains"
)

var (
	images []domains.Imgobj
	urlpost string
)

func Get(urlstr string, quant int, randp bool) ([]domains.Imgobj, error) {

	if randp {

		urlpost = urlstr + "/getimagesrand"

	} else {
		urlpost = urlstr + "/getimages"

	}

	log.Println(urlpost)

	// data, err := url.ParseQuery(`x=1&y=2&y=3;z`)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	
    // // data.Set("title", "Clojure")
    // // data.Set("body", `Clojure is a dynamic programming language that targets the Java Virtual Machine 
    // //     (and the CLR, and JavaScript). It is designed to be a general-purpose language, 
    // //     combining the approachability and interactive development of a 
    // //     scripting language with an efficient and robust infrastructure for multithreaded programming.
    // //     Clojure is a compiled language - it compiles directly to JVM bytecode, 
    // //     yet remains completely dynamic. Every feature supported by Clojure is supported at runtime.`)

    // b := bytes.NewBufferString(data.Encode())


	req, err := http.NewRequest("POST", urlpost, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-QUANT", strconv.Itoa(quant))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}
	r, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer r.Body.Close()

	json.NewDecoder(r.Body).Decode(&images)

	return images, nil

}
