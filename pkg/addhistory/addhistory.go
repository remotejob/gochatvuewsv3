package addhistory

import (
	"time"

	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
)

func Elab(conf *configwsserver.Config, id string, nguid string, msgmap map[string]interface{}) error {

	wsclient := conf.Wsclients[nguid]
	wsclient.Status = "addhistory"
	wsclient.Updated = time.Now()
	wsclient.Img =  msgmap["imgid"].(string)
	// wsclient.Img = msg.Message
	conf.Wsclients[nguid] = wsclient

	return nil

}
