package handlerMsg

import (
	"log"

	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
)

func Elab(conf *configwsserver.Config) {

	for {
		msg := <-conf.Broadcast
		// log.Println("handlerMsg",msg)
		if len(msg.Nguid) > 0 {
			// log.Println("handlerMsg",msg)
			if _, ok := conf.Wsclients[msg.Nguid]; ok {
				// log.Println("handleMSG",msg)
				err := conf.Wsclients[msg.Nguid].Ws.WriteJSON(msg)
				if err != nil {
					log.Printf("handle Message error ID?: %v", err)
					log.Println("msg ?", msg)
					delete(conf.Wsclients, msg.Nguid)

				}
			}
		}

	}
}
