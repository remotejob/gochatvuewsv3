package handleroldclient

import (
	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
)

func Elab(conf *configwsserver.Config, nguid string) error {


	wsclient := conf.Wsclients[nguid]
	wsclient.Status = "answer"
	wsclient.Newcl = false
	conf.Wsclients[nguid] = wsclient

	return nil
}
