package handlerstart

import (
	"time"

	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
	"gitlab.com/remotejob/gochatvuewsv3/internal/domains"
)

var (
	client domains.Client
	newcl bool
	err error
)


func Elab(conf *configwsserver.Config, id string,nguid string, msgmap map[string]interface{}) error {


	// img := msgmap["img"].(domains.Imgobj)

	wsclient := conf.Wsclients[nguid]
	wsclient.Status = "start"
	wsclient.Updated = time.Now()
	wsclient.Img = conf.Defaultimg.ID
	conf.Wsclients[nguid] = wsclient

	return nil
}
