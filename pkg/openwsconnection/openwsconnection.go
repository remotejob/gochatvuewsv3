package openwsconnection

import (
	"encoding/json"

	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
	"gitlab.com/remotejob/gochatvuewsv3/internal/domains"
)

func Elab(conf *configwsserver.Config, id string,nguid string, msgmap map[string]interface{}) error {

	// println("ELAB OPEN Client")

	// var msg domains.Message
	img,_ := json.Marshal(conf.Defaultimg)


	var msg domains.Broadcastmsg
	msg.ID = id
	msg.Nguid = nguid
	msg.Type = "open"
	msg.Payload = string(img)

	conf.Broadcast <- msg

	return nil
}
