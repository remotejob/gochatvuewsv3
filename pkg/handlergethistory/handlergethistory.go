package handlergethistory

import (
	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
)

func Elab(conf *configwsserver.Config,  id string, nguid string, msgmap map[string]interface{}) error {

	wsclient := conf.Wsclients[nguid]
	// wsclient.History = true
	wsclient.Status = "gethistory"
	conf.Wsclients[nguid] = wsclient

	return nil
}
