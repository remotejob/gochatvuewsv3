package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
	"gitlab.com/remotejob/gochatvuewsv3/internal/domains"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/addhistory"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/answerml"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/askml"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/handlerads"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/handlergethistory"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/handlerhistoryok"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/handlernewclient"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/handlernewimg"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/handleroldclient"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/handlersendimgs"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/handlerstart"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/openwsconnection"
	"gitlab.com/remotejob/gochatvuewsv3/pkg/sendimgs"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type Config struct {
	*configwsserver.Config
}

func New(configuration *configwsserver.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Routes() *chi.Mux {

	router := chi.NewRouter()

	router.Get("/ws/{nguid}", config.HandleConnections)
	router.Get("/allclients", config.Getallclients)
	router.Post("/sendmsg", config.Sendmsg)
	router.Get("/getimg/{imgid}", config.Getimg)

	return router

}

func (config *Config) Getimg(w http.ResponseWriter, r *http.Request) {

	imgid := chi.URLParam(r, "imgid")

	for _, img := range config.Imgs {

		if img.ID == imgid {

			resbyte, err := json.Marshal(img)
			if err != nil {

				log.Fatalln(err)
			}

			w.Header().Set("Content-Type", "application/json")

			w.Write(resbyte)

		}
	}

}

func (config *Config) Getallclients(w http.ResponseWriter, r *http.Request) {

	resbyte, err := json.Marshal(config.Wsclients)
	if err != nil {

		log.Fatalln(err)
	}

	w.Header().Set("Content-Type", "application/json")

	w.Write(resbyte)

}

func (config *Config) Sendmsg(w http.ResponseWriter, r *http.Request) {

	var msg domains.Broadcastmsg

	err := json.NewDecoder(r.Body).Decode(&msg)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// log.Println("sendmsg", msg)
	config.Broadcast <- msg

}

func (conf *Config) HandleConnections(w http.ResponseWriter, r *http.Request) {

	var nguid string
	nguid = chi.URLParam(r, "nguid")
	log.Println("nguid", nguid)

	if len(nguid) > 15 {

		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Printf("error %v", err)
			return
		}

		defer func() {
			ws, err := upgrader.Upgrade(w, r, nil)
			if err != nil {
				log.Printf("error %v", err)
				return
			}

			defer func() {

				ws.Close()
				delete(conf.Wsclients, nguid)

			}()

			ws.Close()
			delete(conf.Wsclients, nguid)

		}()

		now := time.Now()
		wsclient := domains.Wsclient{
			Created: now,
			Updated: now,
			Ws:      ws,
			History: false,
			Img:     conf.Defaultimg.ID,
			Status:  "open",
			Ad:      0,
			Wakeup:  0,
		}

		conf.Wsclients[nguid] = wsclient

		for {
			var msg domains.Message
			err := ws.ReadJSON(&msg)
			if err != nil {
				log.Printf("delete client error: %v", err)
				delete(conf.Wsclients, nguid)
				break
			}

			if msg.Type == "open" {

				openwsconnection.Elab(conf.Config, "", nguid, nil)

			}

			if msg.Type == "start" {

				handlerstart.Elab(conf.Config, "", nguid, nil)

			}

			if msg.Type == "oldclient" {

				handleroldclient.Elab(conf.Config, nguid)

			}

			if msg.Type == "newclient" {

				handlernewclient.Elab(conf.Config, "", nguid, nil)

			}

			if msg.Type == "sendimages" {

				sendimgs.Send(conf.Config, "", nguid, nil)

			}
			if msg.Type == "ask" {

				err := askml.Elab(conf.Config, msg)
				if err != nil {

					log.Panicln(err)
				}

			}

			if msg.Type == "answer" {

				log.Println("answer", msg.Message)

				m := make(map[string]interface{})

				err := json.Unmarshal([]byte(msg.Message), &m)
				if err != nil {
					panic(err)
				}

				err = answerml.Elab(conf.Config, "", nguid, m)
				if err != nil {

					log.Panicln(err)
				}

			}

			if msg.Type == "addhistory" {

				log.Println("addhistor", msg.Message)

				m := make(map[string]interface{})
				m["imgid"] = msg.Message
				err := addhistory.Elab(conf.Config, "", nguid, m)
				if err != nil {

					log.Panicln(err)
				}

			}

			if msg.Type == "newimg" {
				log.Println("newimg", msg)

				err := handlernewimg.Elab(conf.Config, "", nguid, nil)
				if err != nil {

					log.Panicln(err)
				}

			}

			if msg.Type == "historyok" {
				log.Println("okhistory!!", nguid, msg.Nguid)

				err := handlerhistoryok.Elab(conf.Config, "", nguid, nil)
				if err != nil {

					log.Panicln(err)
				}

			}

			if msg.Type == "sendimgs" {
				log.Println("sendimgs", nguid)
				err := handlersendimgs.Elab(conf.Config, "", nguid, nil)
				if err != nil {

					log.Panicln(err)
				}

			}

			if msg.Type == "passive" {
				log.Println("passive", nguid)

			}
			if msg.Type == "ads" {
				log.Println("ads", nguid)

				err := handlerads.Elab(conf.Config, "", nguid, nil)
				if err != nil {

					log.Panicln(err)
				}

			}

			if msg.Type == "gethistory" {
				log.Println("gethistory", nguid)

				err := handlergethistory.Elab(conf.Config, "", nguid, nil)
				if err != nil {

					log.Panicln(err)
				}

			}

		}

	}

}
