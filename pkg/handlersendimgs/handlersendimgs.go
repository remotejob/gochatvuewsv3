package handlersendimgs

import (
	"encoding/json"

	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
	"gitlab.com/remotejob/gochatvuewsv3/internal/domains"
)

func Elab(conf *configwsserver.Config, id string, nguid string, msgmap map[string]interface{}) error {

	wsclient := conf.Wsclients[nguid]
	wsclient.Status = "sendimgs"
	// wsclient.Updated = time.Now()
	// wsclient.Img = conf.Defaultimg.ID
	conf.Wsclients[nguid] = wsclient

	img, _ := json.Marshal(conf.Imgs)

	var msg domains.Broadcastmsg
	msg.ID = id
	msg.Nguid = nguid
	msg.Type = "sendimgs"
	msg.Payload = string(img)

	conf.Broadcast <- msg


	return nil
}
