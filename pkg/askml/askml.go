package askml

import (
	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
	"gitlab.com/remotejob/gochatvuewsv3/internal/domains"
)

func Elab(conf *configwsserver.Config, msg domains.Message) error {

	wsclient := conf.Wsclients[msg.Nguid]
	wsclient.Status = "ask"
	wsclient.Ask = msg.Message

	conf.Wsclients[msg.Nguid] = wsclient


	return nil

}
