package handlernewimg

import (
	"time"

	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
)

func Elab(conf *configwsserver.Config, id string, nguid string, msgmap map[string]interface{}) error {

	wsclient := conf.Wsclients[nguid]
	wsclient.Status = "newimg"
	wsclient.Updated = time.Now()
	// wsclient.Img = conf.Defaultimg.ID
	conf.Wsclients[nguid] = wsclient

	return nil
}
