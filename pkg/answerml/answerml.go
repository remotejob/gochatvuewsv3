package answerml

import (
	"time"

	"gitlab.com/remotejob/gochatvuewsv3/internal/configwsserver"
)

func Elab(conf *configwsserver.Config, id string, nguid string, msgmap map[string]interface{}) error {

	// log.Println( msgmap)
	wsclient := conf.Wsclients[nguid]
	wsclient.Status = "answer"
	wsclient.Updated = time.Now()
	wsclient.Lastask = msgmap["lastask"].(string)
	wsclient.Lastanswer = msgmap["lastanswer"].(string)

	conf.Wsclients[nguid] = wsclient

	return nil

}
